# TATools

An R package that contains useful tools for text analytics

### Prerequisites

```
install.packages("devtools")
```

### Installing

You can install the package directly from bitbucket using the following commands.

```
library(devtools)

install_bitbucket("miretxuzf/tatools")
```

## Running

After installing the package as above, import the package by:

```
library(TATools)
```

You can get documentations about the functions by, for example:

```
help(MSD)
help(plot_doc_similarity)
```

## Authors

* **Mire Fukumoto Zhao** - *Initial work*

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License

## Acknowledgments

*Thierry Bertin-Mahieux, Daniel P.W. Ellis, Brian Whitman, and Paul Lamere. The Million Song Dataset. In Proceedings of the 12th International Society for Music Information Retrieval Conference (ISMIR 2011), 2011.
